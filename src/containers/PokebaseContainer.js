import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { GET_POKEMONS } from "graphql/get-pokemons"
import { Waypoint } from "react-waypoint"
import InputLabel from '@material-ui/core/InputLabel';
import Pokebase from "components/Pokebase"
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { useState, useEffect  } from 'react';
import { useHistory, useLocation  } from 'react-router-dom';

export function PokebaseContainer() {
    let history = useHistory();
    let location = useLocation();
    let [pokemonList, setPokemonList] = React.useState([]);
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    let limit = 15;
    if(location.search.split("=").length > 1) {
        limit =  150;
    } 
    const { data, fetchMore } = useQuery(GET_POKEMONS, {
        variables: {first: limit}
    })
    useEffect(() => {
        function init() {
            const filter = location.search.split("=").length > 1 ? location.search.split("=")[1] : ""
            if( filter !== "" && data && data.pokemons.length > 0) {
                data.pokemons = data.pokemons.filter(pokemon => {
                    return pokemon.types && ( pokemon.types.indexOf(filter) !== -1 )
                });
            }
            if(data && data.pokemons.length > 0) {
                setPokemonList(data.pokemons);
                setLoading(false);
            }
        }
        init()
    }, [data]);

    function handleChange(e) {
        history.push(`/?filter=${e.target.value}`)
    }
    return (
            
        <React.Fragment>
            {
                (loading || pokemonList.length === 0) && 
                <Grid container spacing={3} className="pokemon-area__centered">
                    <CircularProgress color="secondary" />
                </Grid>
            }
            
            {
                !loading && pokemonList && pokemonList.length > 0 &&
                <Grid container spacing={3} className="pokemon-area">
                    <Grid item xs={12}>
                        <InputLabel id="demo-simple-select-label">Type</InputLabel>
                        <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={location.search.split("=").length > 1 ? location.search.split("=")[1] : "Normal"}
                        onChange={(e) => handleChange(e)}
                        >
                            <MenuItem value={"Normal"}>Normal</MenuItem>
                            <MenuItem value={"Fire"}>Fire</MenuItem>
                            <MenuItem value={"Fighting"}>Fighting</MenuItem>
                            <MenuItem value={"Water"}>Water</MenuItem>
                            <MenuItem value={"Flying"}>Flying</MenuItem>
                            <MenuItem value={"Grass"}>Grass</MenuItem>
                            <MenuItem value={"Poison"}>Poison</MenuItem>
                            <MenuItem value={"Electric"}>Electric</MenuItem>
                            <MenuItem value={"Ground"}>Ground</MenuItem>
                            <MenuItem value={"Psychic"}>Psychic</MenuItem>
                            <MenuItem value={"Rock"}>Rock</MenuItem>
                        </Select>
                    </Grid>
                    {pokemonList.map((pokemon, key) => 
                        (
                            <React.Fragment key={pokemon.id}>
                                <Grid item lg={4} xs={12}>
                                    <a className="pokebase-link" href={`pokebase/${pokemon.name}`}>
                                        <Pokebase pokemon={pokemon}/>   
                                    </a>
                                </Grid>
                                {key === pokemonList.length - 1 && pokemonList.length < 149 && location.search.split("=").length === 1 && 
                                    (
                                        <React.Fragment>
                                            <Waypoint onEnter={() => fetchMore({
                                                    variables: {
                                                        first: (page+1) * 15
                                                    },
                                                    updateQuery: (pv, {fetchMoreResult}) => {
                                                        setPage(page+1)
                                                        if(!fetchMoreResult) {
                                                            return pv;
                                                        }
                                                    return {
                                                            __typename: "Pokemon",
                                                            pokemons: [
                                                                ...fetchMoreResult.pokemons
                                                            ]
                                                    }
                                                    }
                                                })
                                            } 
                                            />
                                            <Grid container spacing={3} className="pokemon-area__centered">
                                                <CircularProgress color="secondary" />
                                            </Grid>
                                        </React.Fragment>
                                    )

                                }
                            </React.Fragment>
                        )
                    )}
                </Grid>
            }
        </React.Fragment>
    )
}