import React from "react"
import { useQuery } from "@apollo/react-hooks"
import { GET_POKEMON } from "graphql/get-pokemon"
import Pokebase from "components/Pokebase"
import Grid from '@material-ui/core/Grid';
import { useParams, Link } from "react-router-dom";
import CircularProgress from '@material-ui/core/CircularProgress';

export function PokebaseDetailContainer() {
    let { name } = useParams();
    
    const [pokemonObject, setPokemonObject] = React.useState([]);

    const { data: { pokemon = {} } = {} } = useQuery(GET_POKEMON, {
        variables: {id: "", name: name}
    })
    setTimeout(() => {
        setPokemonObject(pokemon);
    }, 500);

    return (

        <React.Fragment>

            {
                (!pokemonObject || !pokemonObject.id) && 
                <Grid container spacing={3} className="pokemon-area__centered">
                    <CircularProgress color="secondary" />
                </Grid>
            }
            {pokemonObject && pokemonObject.id &&
                <Grid container spacing={3} className="pokemon-area">
                    <Grid item lg={4}>
                        <Pokebase pokemon={pokemonObject}/> 
                        {
                            pokemonObject.evolutions && pokemonObject.evolutions.length > 0 && 
                            <Grid container className="element-evolution">
                                <Grid item xs={12} className="evolution-area">
                                    <div className="element-title">Evolutions</div>
                                    <div className="evolution-list">
                                        {   
                                            pokemonObject.evolutions.map(p => {
                                                return (
                                                    <Grid item xs={6} key={p.name}>
                                                    <Link to={{pathname: `/pokebase/${p.name}`}}>
                                                            <Pokebase pokemon={p} smallSize={"true"}/> 
                                                    </Link>
                                                    </Grid>
                                                )
                                            })
                                        }
                                    </div>
                                </Grid>
                            </Grid>
                        }
                    </Grid>
                    <Grid item lg={8}>
                        <Grid container>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Weight range</div>
                                <div className="element-content">{pokemonObject.weight.minimum} -  {pokemonObject.weight.maximum}</div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Height range</div>
                                <div className="element-content">{pokemonObject.height.minimum} -  {pokemonObject.height.maximum}</div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Classification</div>
                                <div className="element-content">{pokemonObject.classification}</div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Types</div>
                                <div className="element-content">{pokemonObject.types.join(',')}</div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Fast Attacks</div>
                                <div className="element-content">
                                    <ul>
                                    {pokemonObject.attacks && pokemonObject.attacks.fast && 
                                        pokemonObject.attacks.fast.map(attack => {
                                            return (<li key={attack.name}> {attack.name} ({attack.type}) </li>)
                                        })
                                    }
                                    </ul>
                                </div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Special Attacks</div>
                                <div className="element-content">
                                    <ul>
                                    {pokemonObject.attacks && pokemonObject.attacks.special && 
                                        pokemonObject.attacks.special.map(attack => {
                                            return (<li key={attack.name}> {attack.name} ({attack.type}) </li>)
                                        })
                                    }
                                    </ul>
                                </div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Resistance</div>
                                <div className="element-content">
                                    <ul>
                                    {pokemonObject.resistant && pokemonObject.resistant && 
                                        pokemonObject.resistant.map(resistant => {
                                            return (<li key={resistant}> {resistant} </li>)
                                        })
                                    }
                                    </ul>
                                </div>
                            </Grid>
                            <Grid item xs={12} className="element">
                                <div className="element-title">Weaknesses</div>
                                <div className="element-content">
                                    <ul>
                                    {pokemonObject.weaknesses && pokemonObject.weaknesses && 
                                        pokemonObject.weaknesses.map(weakness => {
                                            return (<li key={weakness}> {weakness} </li>)
                                        })
                                    }
                                    </ul>
                                </div>
                            </Grid>
                            
                        </Grid>
                    </Grid>
                </Grid>
            }
        </React.Fragment>
    )
}