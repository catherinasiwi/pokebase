import React from "react"
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { PokebaseContainer } from "containers/PokebaseContainer"

export function Home() {
    const client = new ApolloClient({
        uri: "https://graphql-pokemon2.vercel.app/"
    })
    return (
        <ApolloProvider client={client}>
            <PokebaseContainer />
        </ApolloProvider>
    )
}