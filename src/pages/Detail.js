import React from "react"
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";
import { PokebaseDetailContainer } from "containers/PokebaseDetailContainer"

export function Detail() {
    const client = new ApolloClient({
        uri: "https://graphql-pokemon2.vercel.app/"
    })
    return (
        <ApolloProvider client={client}>
            <PokebaseDetailContainer />
        </ApolloProvider>
    )
}