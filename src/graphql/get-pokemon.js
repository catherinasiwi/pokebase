import gql from "graphql-tag"

export const GET_POKEMON = gql `
    query pokemon($id: String, $name: String) {
        pokemon(id: $id, name: $name) {
            id
            name
            image
            weight {
                minimum
                maximum
            }
            height {
                minimum
                maximum
            }
                classification
                types
                resistant
                attacks {
                fast {
                name
                type
                damage
                }
                special {
                name
                type
                damage
                }
            }
                weaknesses
                evolutions {
                    name
                    image
                }
        }
    }
`