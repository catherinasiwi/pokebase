import React from "react" 
import Grid from '@material-ui/core/Grid';

class Pokebase extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Grid item xs={12} className="pokebase-card" key={this.props.pokemon.name}>
                    <div className={`pokemon-image-area ${this.props.smallSize === "true" ? 'small' : ''}`}>
                        <img alt={this.props.pokemon.image} src={this.props.pokemon.image} className="pokemon-image"/>
                    </div>
                    <div className="pokemon-name">
                        {this.props.pokemon.name ? this.props.pokemon.name : this.props.pokemon}
                    </div>
                </Grid>
            </React.Fragment>
        )
    }
}

export default Pokebase