import React from 'react'
import { Route, Switch } from "react-router-dom";
import { Home } from 'pages/Home';
import { Detail } from 'pages/Detail';

const ROUTES = [
    { path: "/", key: "ROOT", exact: true, component: () => <Home /> },
    { path: "/pokebase/:name", key: "ROOT", exact: true, component: () => <Detail /> },
]

export default ROUTES;

export function RenderRoutes({ routes }) {
    return (
        <Switch>
        {routes.map((route, i) => {
            return <RouteWithSubRoutes key={route.key} {...route} />;
        })}
        <Route component={() => <h1>Not Found!</h1>} />
        </Switch>
    );
}
function RouteWithSubRoutes(route) {
    return (
        <Route
        path={route.path}
        exact={route.exact}
        render={props => <route.component {...props} routes={route.routes} />}
        />
    );
}