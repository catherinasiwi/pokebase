import './Pokemon.css';
import Container from '@material-ui/core/Container';
import ROUTES, { RenderRoutes } from "./routes/main";

function App() {
  return (
    <div className="pokebase">
      <header className="pokebase__header">
        <a href="/">
          <img src="../images/pokebase-logo.png" className="pokebase__header__logo" alt="logo" />
        </a>
      </header>
      <Container maxWidth="md" className="pokebase__main-container">
        <RenderRoutes routes={ROUTES} />
      </Container>
    </div>
  );
}

export default App;
